﻿export interface IUser {
  id?: number;
  username?: string;
  fullname?: string;
  password?: string;
  phoneNumber?: string;
  email?: string;
  isApproved?: boolean;
  isDeleted?: boolean;
  birthday?: Date;
  roles?: string[];
}

export class User implements IUser {
   constructor(public id?: number,
               public username?: string,
               public fullname?: string,
               public password?: string,
               public phoneNumber?: string,
               public email?: string,
               public isApproved?: boolean,
               public isDeleted?: boolean,
               public birthday?: Date,
               public roles?: string[]
   ) {
  }
}
