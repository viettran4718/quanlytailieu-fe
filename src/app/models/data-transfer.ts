export interface IDataTransfer<T>{
  status?: number;
  isError?: boolean;
  message?: string;
  data?: T;
  otherInformation?: any;
}

export class DataTransfer<T> implements IDataTransfer<T> {
  constructor(public status?: number,
              public isError?: boolean,
              public message?: string,
              public data?: T,
              public otherInformation?: any,
  ) {
  }
}
