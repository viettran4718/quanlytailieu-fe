export interface IDocument {
  id?: number;
  title?: string;
  content?: string;
  file?: string;
  publishDate?: Date;
  startDate?: Date;
  endDate?: Date;
}

export class Document implements IDocument {
   constructor(public id?: number,
               public title?: string,
               public content?: string,
               public file?: string,
               public publishDate?: Date,
               public startDate?: Date,
               public endDate?: Date
   ) {
   }
}
