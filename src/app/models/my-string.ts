export interface IMyString {
  data?: string;
}

export class MyString implements IMyString {
  public constructor(
    public data?: string,
  ) {
  }
}
