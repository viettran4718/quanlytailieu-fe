export interface IBaseEntity {
  createDate?: Date;
  createBy?: string;
  modifiedDate?: Date;
  modifiedBy?: string;
}

export class BaseEntity implements IBaseEntity {
  constructor(public createDate?: Date,
              public createBy?: string,
              public modifiedDate?: Date,
              public modifiedBy?: string,
  ) {
  }
}
