import {IUser} from './user';

export interface IAccount {
  id?: number;
  accountNumber?: string;
  accountBalance?: number;
  expiredDate?: Date;
  accountOwner?: IUser;
}

export class Account implements IAccount {
  constructor(public id?: number,
              public accountNumber?: string,
              public accountBalance?: number,
              public expiredDate?: Date,
              public accountOwner?: IUser,
  ) {
  }
}
