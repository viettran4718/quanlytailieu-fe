export interface ICbbObj {
  id?: number;
  value?: string;
  text?: string;
  extendValue?: string;
  description?: string;
}

export class CbbObj implements ICbbObj {
  constructor(public id?: number,
              public value?: string,
              public text?: string,
              public extendValue?: string,
              public description?: string,
  ) {
  }
}
