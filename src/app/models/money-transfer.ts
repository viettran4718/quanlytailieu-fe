import {IAccount} from './account';
import {BaseEntity, IBaseEntity} from './baseEntity';

export interface IMoneyTransfer extends IBaseEntity{
  id?: number;
  tradeCode?: number;
  note?: string;
  amount?: number;
  status?: number;
  sender?: IAccount;
  receiver?: IAccount;

}

export class MoneyTransfer implements IMoneyTransfer{
  constructor(public id?: number,
              public tradeCode?: number,
              public note?: string,
              public amount?: number,
              public status?: number,
              public sender?: IAccount,
              public receiver?: IAccount,
              public createDate?: Date,
              public createBy?: string,
              public modifiedDate?: Date,
              public modifiedBy?: string,
  ) {
  }
}
