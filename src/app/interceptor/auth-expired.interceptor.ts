import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';

import {AuthenticationService} from '../service/authentication.service';
import {LoginModalService} from '../component/login/login-modal.service';

@Injectable()
export class AuthExpiredInterceptor implements HttpInterceptor {
  constructor(
    private authenticationService: AuthenticationService,
    private loginModalService: LoginModalService,
    private router: Router
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      tap(null, (err: HttpErrorResponse) => {
        if (err.status === 401 && err.url && !err.url.includes('user/api/login')) {
          this.authenticationService.logout();
          this.router.navigate(['']);
          this.loginModalService.open();
        }
        if (err.status === 403) {
          this.authenticationService.logout();
          this.router.navigate(['permission-deny']);
        }
      })
    );
  }
}
