import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';

import {SERVER_API_URL} from '../shared/constant/app.constants';
import {RsaKey} from '../shared/constant/rsa-key';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor() {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log(request.url);
    if (!request || !request.url || (request.url.startsWith('http') && request.url.endsWith('/api/user/login'))) {
      return next.handle(request);
    }
    const token = sessionStorage.getItem('authenticationToken');
    // const rsaKey = new RsaKey();
    console.log(RsaKey.getPublicKey());
    if (token) {
      request = request.clone({
        setHeaders: {
          publicKey: RsaKey.getPublicKey(),
          Authorization: 'Bearer ' + token
        },
      });
    }
    return next.handle(request);
  }
}
