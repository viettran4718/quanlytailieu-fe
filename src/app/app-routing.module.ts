import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {BrowserModule} from '@angular/platform-browser';

import {LoginComponent} from './component/login/login.component';
import {RegisterComponent} from './component/register/register.component';
import {PageNotFoundComponent} from './component/page-not-found/page-not-found.component';
import {HomeComponent} from './component/home/home.component';
import {UsersComponent} from './component/users/users.component';
import {UserAddComponent} from './component/users/user-add.component';
import {UserUpdateComponent} from './component/users/user-update.component';
import {UserDetailComponent} from './component/users/user-detail.component';
import {LogoutComponent} from './component/logout/logout.component';
import {MyInfoComponent} from './component/my-info/my-info.component';
import {PermissionDenyComponent} from './component/permission-deny/permission-deny.component';
import {DocumentComponent} from './component/document/document.component';
import {DocumentAddComponent} from './component/document/document-add.component';
import {MoneyTransferComponent} from './component/money-transfer/money-transfer.component';
import {MoneyTransferAddComponent} from './component/money-transfer/money-transfer-add.component';
import {MoneyTransferUpdateComponent} from './component/money-transfer/money-transfer-update.component';
import {MoneyTransferDetailComponent} from './component/money-transfer/money-transfer-detail.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LogoutComponent},
  {path: 'myinfo', component: MyInfoComponent},

  {path: 'permission-deny', component: PermissionDenyComponent},
  {path: 'home', component: HomeComponent},
  {path: 'register', component: RegisterComponent},

  {path: 'user', component: UsersComponent},
  {path: 'user/new', component: UserAddComponent},
  {path: 'user/update/:id', component: UserUpdateComponent},
  {path: 'user/view/:id', component: UserDetailComponent},

  {path: 'document', component: DocumentComponent},
  {path: 'document/new', component: DocumentAddComponent},
  {path: 'document/update/:id', component: UserUpdateComponent},
  {path: 'document/view/:id', component: UserDetailComponent},

  {path: 'money-transfer', component: MoneyTransferComponent},
  {path: 'money-transfer/new', component: MoneyTransferAddComponent},
  {path: 'money-transfer/update/:id', component: MoneyTransferUpdateComponent},
  {path: 'money-transfer/view/:id', component: MoneyTransferDetailComponent},


  {path: '**', component: PageNotFoundComponent},  // Wildcard route for a 404 page
];

@NgModule({
  imports: [
    HttpClientModule,
    BrowserModule,
    RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
