import {Injectable} from '@angular/core';

declare var JSEncrypt;

@Injectable()
export class RsaKey {
  // static instance: RsaKey;

  private static publicKey: string;

  private static privateKey: string;

  // public serverPublicKey: string;
  // constructor() {
  //   console.log('RSA key constructor');
  //   if (!this.publicKey || !this.privateKey) {
  //     this.privateKey = this.removeHeaderKey(crypt.getPrivateKey());
  //     this.publicKey = this.removeHeaderKey(crypt.getPublicKey());
  //   }
  //   console.log(this.privateKey);
  //   console.log(this.publicKey);
  // }
  public static setPublicKey(publicKey: string) {
    this.publicKey = publicKey;
  }

  public static setPrivateKey(privateKey: string) {
    this.privateKey = privateKey;
  }

  public static getPublicKey() {
    return this.publicKey;
  }

  public static getPrivateKey() {
    return this.privateKey;
  }

  public static getServerPublicKey(){
    return sessionStorage.getItem('publicKeyServer');
  }

  public static removeHeaderKey(keyFull: string) {
    let key = keyFull.replace('-----BEGIN RSA PUBLIC KEY-----', '');
    key = key.replace('-----END RSA PUBLIC KEY-----', '');
    key = key.replace('-----BEGIN RSA PRIVATE KEY-----', '');
    key = key.replace('-----END RSA PRIVATE KEY-----', '');

    key = key.replace('-----BEGIN PUBLIC KEY-----', '');
    key = key.replace('-----END PUBLIC KEY-----', '');
    key = key.replace('-----BEGIN PRIVATE KEY-----', '');
    key = key.replace('-----END PRIVATE KEY-----', '');
    key = key.replace(/(\r\n|\n|\r)/gm, '').trim();
    return key;
  }
}
