import {Injectable} from '@angular/core';

@Injectable()
export class DataBuilderUtil {
  private static specialSymbol = '!!!@@@';

  private aesKeyEncrypted: string;
  private dataEncrypted: string;

  static buildData(aesKeyEncrypted: string, dataEncrypted: string) {
    return aesKeyEncrypted + this.specialSymbol + dataEncrypted;
  }

  constructor(mixData: string) {
    if (this.aesKeyEncrypted === undefined) {
      this.aesKeyEncrypted = null;
    }
    if (this.dataEncrypted === undefined) {
      this.dataEncrypted = null;
    }
    const split = mixData.split(DataBuilderUtil.specialSymbol);
    try {
      this.aesKeyEncrypted = split[0];
      this.dataEncrypted = split[1];
    } catch (e) {
      console.error(e.message, e);
    }
  }

  public getAesKeyEncrypted(): string {
    return this.aesKeyEncrypted;
  }
  public getDataEncrypted(): string {
    return this.dataEncrypted;
  }
}
