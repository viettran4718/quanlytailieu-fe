import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Event, NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  path: string;
  isInit = true;

  constructor(private router: Router) {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd ) {
        this.path = window.location.href;
        this.isInit = false;
      }
    });
  }

  ngOnInit(): void {
    this.isInit = true;

  }


}
