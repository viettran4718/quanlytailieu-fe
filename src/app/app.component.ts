import {Component, HostListener, OnInit} from '@angular/core';
import {RsaKey} from './shared/constant/rsa-key';
import {Router} from '@angular/router';

declare var JSEncrypt;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {


  constructor(private router: Router) {
  }

  ngOnInit() {
    if (!sessionStorage.getItem('authenticationToken')) {
      this.router.navigate(['/login']);
      return;
    }
    if (!sessionStorage.getItem('publicKey') || !sessionStorage.getItem('privateKey')) {
      const crypt = new JSEncrypt({default_key_size: 1024});
      RsaKey.setPublicKey(RsaKey.removeHeaderKey(crypt.getPublicKey()));
      RsaKey.setPrivateKey(RsaKey.removeHeaderKey(crypt.getPrivateKey()));
    } else {
      RsaKey.setPublicKey(sessionStorage.getItem('publicKey'));
      RsaKey.setPrivateKey(sessionStorage.getItem('privateKey'));
    }
    sessionStorage.removeItem('publicKey');
    sessionStorage.removeItem('privateKey');

    console.log(RsaKey.getPublicKey());
    console.log(RsaKey.getPrivateKey());
  }


  // close tab and reload listen
  @HostListener('window:beforeunload', ['$event'])
  beforeUnloadHander(event) {
    this.saveKeyToStorage();
  }

  private saveKeyToStorage() {
    // const href = window.location.href;
    if (RsaKey.getPrivateKey() && RsaKey.getPublicKey()) {
      sessionStorage.setItem('privateKey', RsaKey.getPrivateKey());
      sessionStorage.setItem('publicKey', RsaKey.getPublicKey());
    }
  }
}
