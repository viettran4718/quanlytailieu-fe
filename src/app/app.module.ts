import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {RegisterComponent} from './component/register/register.component';
import {LoginComponent} from './component/login/login.component';
import {PageNotFoundComponent} from './component/page-not-found/page-not-found.component';
import {AlertComponent} from './component/alert/alert.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HomeComponent} from './component/home/home.component';
import {UsersComponent} from './component/users/users.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthInterceptor} from './interceptor/auth.interceptor';
import {AuthExpiredInterceptor} from './interceptor/auth-expired.interceptor';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {CommonModule} from '@angular/common';
import {UserAddComponent} from './component/users/user-add.component';
import {MatInputModule} from '@angular/material/input';
import {UserDeleteDialogComponent} from './component/users/user-delete-dialog.component';
import {ErrorHandlerInterceptor} from './interceptor/errorhandler.interceptor';
import {UserUpdateComponent} from './component/users/user-update.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {UserDetailComponent} from './component/users/user-detail.component';
import {HeaderComponent} from './layout/header/header.component';
import {FooterComponent} from './layout/footer/footer.component';
import {MatMenuModule} from '@angular/material/menu';
import {MyInfoComponent} from './component/my-info/my-info.component';
import {LogoutComponent} from './component/logout/logout.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {PermissionDenyComponent} from './component/permission-deny/permission-deny.component';
import {DocumentComponent} from './component/document/document.component';
import {DocumentAddComponent} from './component/document/document-add.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatPaginatorModule} from '@angular/material/paginator';
import {ToastrModule} from 'ngx-toastr';
import {AccountComponent} from './component/account/account.component';
import {MoneyTransferComponent} from './component/money-transfer/money-transfer.component';
import {MoneyTransferAddComponent} from './component/money-transfer/money-transfer-add.component';
import {MoneyTransferDeleteDialogComponent} from './component/money-transfer/money-transfer-delete-dialog.component';
import {MoneyTransferUpdateComponent} from './component/money-transfer/money-transfer-update.component';
import {RsaKey} from './shared/constant/rsa-key';
import {MoneyTransferDetailComponent} from './component/money-transfer/money-transfer-detail.component';

const modules = [
  BrowserModule,
  AppRoutingModule,
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  BrowserAnimationsModule,
  NgbModule,
  HttpClientModule,
  FontAwesomeModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatCheckboxModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatPaginatorModule,
  ToastrModule.forRoot()
];

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    PageNotFoundComponent,
    AlertComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    UsersComponent,
    UserAddComponent,
    UserDeleteDialogComponent,
    UserUpdateComponent,
    UserDetailComponent,
    MyInfoComponent,
    LogoutComponent,
    PermissionDenyComponent,
    DocumentComponent,
    DocumentAddComponent,
    AccountComponent,
    MoneyTransferComponent,
    MoneyTransferAddComponent,
    MoneyTransferDeleteDialogComponent,
    MoneyTransferUpdateComponent,
    MoneyTransferDetailComponent
  ],
  imports: [...modules],
  // exports: [PermissionDenyComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthExpiredInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHandlerInterceptor,
      multi: true,
    },
    RsaKey,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
