// import { Injectable } from '@angular/core';
// import { HttpResponse } from '@angular/common/http';
// import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
// import { Observable, of, EMPTY } from 'rxjs';
// import { flatMap } from 'rxjs/operators';
// import {UserService} from './user.service';
// import {IUser, User} from '../../models';
// import {UsersComponent} from './users.component';
// import {UserAddComponent} from './user-update.component';
//
//
// @Injectable({ providedIn: 'root' })
// export class UserResolve implements Resolve<IUser> {
//   constructor(private service: UserService, private router: Router) {}
//
//   resolve(route: ActivatedRouteSnapshot): Observable<IUser> | Observable<never> {
//     const id = route.params['id'];
//     if (id) {
//       return this.service.find(id).pipe(
//         flatMap((User: HttpResponse<User>) => {
//           if (User.body) {
//             return of(User.body);
//           } else {
//             this.router.navigate(['404']);
//             return EMPTY;
//           }
//         })
//       );
//     }
//     return of(new User());
//   }
// }
//
// export const UserRoute: Routes = [
//   {
//     path: '',
//     component: UsersComponent,
//
//     // canActivate: [UserRouteAccessService],
//   },
//   // {
//   //   path: ':id/view',
//   //   component: UserDetailComponent,
//   //   resolve: {
//   //     User: UserResolve,
//   //   },
//   //   // canActivate: [UserRouteAccessService],
//   // },
//   {
//     path: 'new',
//     component: UserAddComponent,
//     resolve: {
//       User: UserResolve,
//     },
//     // data: {
//     //   authorities: [Authority.USER],
//     //   pageTitle: 'Users',
//     // },
//     // canActivate: [UserRouteAccessService],
//   },
//   {
//     path: ':id/edit',
//     component: UserAddComponent,
//     resolve: {
//       User: UserResolve,
//     },
//   //   data: {
//   //     authorities: [Authority.USER],
//   //     pageTitle: 'Users',
//   //   },
//   //   canActivate: [UserRouteAccessService],
//   // },
// ];
