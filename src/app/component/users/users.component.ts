import { Component, OnInit } from '@angular/core';
import {IUser} from '../../models';
import {Subscription} from 'rxjs';
import {UserService} from './user.service';
import {HttpResponse} from '@angular/common/http';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UserDeleteDialogComponent} from './user-delete-dialog.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users?: IUser[];
  eventSubscriber?: Subscription;
  constructor(protected userService: UserService, protected modalService: NgbModal) { }

  loadAll(): void {
    this.userService.query().subscribe((res: HttpResponse<IUser[]>) => (this.users = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IUser): number {
    return item.id;
  }

  delete(user: IUser): void {
    const modalRef = this.modalService.open(UserDeleteDialogComponent, { size: 'md', backdrop: 'static' });
    modalRef.componentInstance.user = user;
  }

}
