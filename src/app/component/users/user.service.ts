import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';

import {SERVER_API_URL} from '../../shared/constant/app.constants';
import {createRequestOption} from '../../shared/util/request-util';
import {IUser} from '../../models';
import {IUserRegister} from '../../models/user-register';

type EntityResponseType = HttpResponse<IUser>;
type EntityResponseType2 = HttpResponse<IUserRegister>;
type EntityArrayResponseType = HttpResponse<IUser[]>;

@Injectable({providedIn: 'root'})
export class UserService {
  public resourceUrl = SERVER_API_URL + '/api/admin/user';

  constructor(protected http: HttpClient) {
  }

  create(user: IUserRegister): Observable<EntityResponseType2> {
    return this.http.post<IUserRegister>(this.resourceUrl, user, {observe: 'response'});
  }

  update(user: IUser): Observable<EntityResponseType> {
    return this.http.put<IUser>(this.resourceUrl, user, {observe: 'response'});
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IUser>(`${this.resourceUrl}/${id}`, {observe: 'response'});
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUser[]>(this.resourceUrl + 's', {params: options, observe: 'response'});
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, {observe: 'response'});
  }


}
