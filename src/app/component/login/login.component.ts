import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';

import {AlertService} from '../../service/alert.service';
import {AuthenticationService} from '../../service/authentication.service';
import {RsaKey} from '../../shared/constant/rsa-key';

declare var JSEncrypt;

@Component({templateUrl: 'login.component.html'})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService
  ) {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.loginForm.patchValue({
      username: 'viettran',
      password: 'viettran',
    });
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // reset alerts on submit
    this.alertService.clear();

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService.login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          // const crypt = new JSEncrypt({default_key_size: 1024});
          // // // RsaKey.publicKey = RsaKey.removeHeaderKey(crypt.getPublicKey());
          // // sessionStorage.setItem('publicKey', RsaKey.removeHeaderKey(crypt.getPublicKey()));
          // // sessionStorage.setItem('privateKey', RsaKey.removeHeaderKey(crypt.getPrivateKey()));
          // // const rsaKey = new RsaKey();
          // // RsaKey.privateKey = RsaKey.removeHeaderKey(crypt.getPrivateKey());
          // // RsaKey.publicKey = RsaKey.removeHeaderKey(crypt.getPublicKey());
          // // // sessionStorage.setItem('privateKey', RsaKey.removeHeaderKey(crypt.getPrivateKey()));
          // // // RsaKey.privateKey = RsaKey.removeHeaderKey(crypt.getPrivateKey());
          // // console.log(RsaKey.privateKey);
          // // console.log(RsaKey.publicKey);
          // RsaKey.setPublicKey(RsaKey.removeHeaderKey(crypt.getPublicKey()));
          // RsaKey.setPrivateKey(RsaKey.removeHeaderKey(crypt.getPrivateKey()));
          // console.log(RsaKey.getPublicKey());
          // console.log(RsaKey.getPrivateKey());
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}
