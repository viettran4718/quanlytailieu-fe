import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  arr = [{
    key: 1,
    value: 'Số 1'
  }, {
    key: 2,
    value: 'Số 2'
  }, {
    key: 3,
    value: 'Số 3'
  }];

  testValue;
  selected = 'option2';

  constructor() {
  }

  ngOnInit(): void {
    console.log(this.testValue);
    console.log(this.arr);
    this.testValue = "2";

  }

}
