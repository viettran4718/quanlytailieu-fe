import {Component, OnInit} from '@angular/core';
import {SearchWithPagination, SearchWithPaginationCls} from '../../shared/util/request-util';
import {IMoneyTransfer} from '../../models/money-transfer';
import {Subscription} from 'rxjs';
import {MoneyTransferService} from './money-transfer.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MoneyTransferDeleteDialogComponent} from './money-transfer-delete-dialog.component';
import {PageEvent} from '@angular/material/paginator';
import {DataBuilderUtil} from '../../shared/util/data-builder-util';
import {AesService} from '../../service/aes.service';
import {RsaService} from '../../service/rsa.service';

@Component({
  selector: 'app-money-transfer',
  templateUrl: './money-transfer.component.html',
  styleUrls: ['./money-transfer.component.css']
})
export class MoneyTransferComponent implements OnInit {

  totalRecords = 0;
  req: SearchWithPagination;
  isLoading = false;
  moneyTransfers?: IMoneyTransfer[];
  eventSubscriber?: Subscription;

  // MatPaginator Inputs
  length = 100;
  pageSize = 10;
  pageSizeOptions = [5, 10, 25, 50];

  // MatPaginator Output
  pageEvent: PageEvent;

  constructor(protected moneyTransferService: MoneyTransferService, protected modalService: NgbModal, private aesService: AesService, private rsaService: RsaService) {
  }

  ngOnInit(): void {
    this.req = new SearchWithPaginationCls();
    this.req.page = 0;
    this.req.size = 5;
    this.loadAll();
  }

  loadAll(): void {
    this.isLoading = true;
    this.moneyTransferService.query(this.req).subscribe(res => {
      if (res && res.body && res.body.data) {
        const dataBuilder = new DataBuilderUtil(res.body.data);
        this.aesService.secretKey = this.rsaService.decrypt(dataBuilder.getAesKeyEncrypted());
        const decryptedData = this.aesService.decrypt(dataBuilder.getDataEncrypted());
        this.moneyTransfers = JSON.parse(decryptedData) || [];
        this.totalRecords = res.body.otherInformation['X-Total-Count'];
        console.log(this.totalRecords);
        console.log(this.moneyTransfers);
      }
      this.isLoading = false;
    });
  }

  trackId(index: number, item: IMoneyTransfer): number {
    return item.id;
  }

  onPaginateChange(pageEvent) {
    this.req.page = pageEvent.pageIndex;
    this.req.size = pageEvent.pageSize;
    this.loadAll();
    return pageEvent;
  }

  delete(moneyTransfer: IMoneyTransfer): void {
    const modalRef = this.modalService.open(MoneyTransferDeleteDialogComponent, {size: 'md', backdrop: 'static'});
    modalRef.componentInstance.user = moneyTransfer;
  }

}
