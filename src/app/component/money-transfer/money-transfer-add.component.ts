import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {HttpResponse} from '@angular/common/http';
import {MoneyTransferService} from './money-transfer.service';
import {IMoneyTransfer, MoneyTransfer} from '../../models/money-transfer';
import {ICbbObj} from '../../models/cbb-obj';
import {AccountService} from '../account/account.service';
import {IAccount} from '../../models/account';
import {AesService} from '../../service/aes.service';
import {RsaService} from '../../service/rsa.service';
import {RsaKey} from '../../shared/constant/rsa-key';
import {DataBuilderUtil} from '../../shared/util/data-builder-util';

@Component({
  selector: 'app-money-transfer-add',
  templateUrl: './money-transfer-add.component.html',
})
export class MoneyTransferAddComponent implements OnInit {
  isSaving = false;
  submitted = false;
  loading = false;

  listAccount: ICbbObj[];

  editForm = this.fb.group({
    id: [],
    sender: ['', Validators.required],
    receiver: ['', Validators.required],
    amount: ['', Validators.required],
    status: ['', Validators.required],
    note: [],
  });

  constructor(protected activatedRoute: ActivatedRoute,
              private fb: FormBuilder,
              protected moneyTransferService: MoneyTransferService,
              protected accountService: AccountService,
              private aesService: AesService,
              private rsaService: RsaService,
  ) {
  }

  ngOnInit(): void {
    // this.activatedRoute.data.subscribe(({moneyTransfer}) => {
    //   this.updateForm(moneyTransfer);
    // });
    this.accountService.getCbbData().subscribe(res => {
      if (res && res.body) {
        this.listAccount = res.body.data;
      }

    });
  }


  previousState(): void {
    window.history.back();
  }

  private createFromForm(): Promise<IMoneyTransfer> {

    return new Promise<IMoneyTransfer>(resolve => {
      const saved = new MoneyTransfer();

      const senderId = this.editForm.get(['sender'])!.value;
      const receiverId = this.editForm.get(['receiver'])!.value;

      this.getAccountFromId(senderId).then(resolve1 => {
        saved.sender = resolve1;
        this.getAccountFromId(receiverId).then(resolve2 => {
          saved.receiver = resolve2;
          saved.amount = this.editForm.get(['amount'])!.value;
          saved.status = this.editForm.get(['status'])!.value;
          saved.note = this.editForm.get(['note'])!.value;
          resolve(saved);
        });
      });
    });

  }

  getAccountFromId(id): Promise<IAccount> {
    return new Promise<IAccount>(resolve => {
      this.accountService.find(id).subscribe(res => {
        if (res && res.body && res.body.data) {
          resolve(res.body.data);
        }
      });
    });
  }

  save(): void {
    if (this.editForm.invalid) {
      return;
    }
    this.isSaving = true;

    this.createFromForm().then(resolve => {
      const dataStr = JSON.stringify(resolve);
      const keyEncrypt = this.rsaService.encrypt(this.aesService.secretKey, RsaKey.getServerPublicKey());
      console.log(RsaKey.getServerPublicKey());
      const dataEncrypt = this.aesService.encrypt(dataStr);

      const sendData = DataBuilderUtil.buildData(keyEncrypt, dataEncrypt);

      this.subscribeToSaveResponse(this.moneyTransferService.create(sendData));
    });
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMoneyTransfer>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  // updateForm(moneyTransfer: IMoneyTransfer): void {
  //   this.editForm.patchValue({
  //     id: moneyTransfer.id,
  //     moneyTransfername: moneyTransfer.moneyTransfername,
  //     password: moneyTransfer.password,
  //     fullname: moneyTransfer.fullname
  //   });
  // }

  get f() {
    return this.editForm.controls;
  }
}
