import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import {IUser} from '../../models';
import {EventManager} from '@angular/platform-browser';
import {MoneyTransferService} from './money-transfer.service';

@Component({
  templateUrl: './money-transfer-delete-dialog.component.html',
})
export class MoneyTransferDeleteDialogComponent {
  user?: IUser;

  constructor(protected moneyTransferService: MoneyTransferService, public activeModal: NgbActiveModal) {
    console.log(this.user);
  }

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.moneyTransferService.delete(id).subscribe(() => {
      this.activeModal.close();
      window.location.reload();
    });
  }
}
