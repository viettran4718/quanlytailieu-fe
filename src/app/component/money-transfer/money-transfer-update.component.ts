import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {HttpResponse} from '@angular/common/http';
import {MoneyTransferService} from './money-transfer.service';
import {IMoneyTransfer} from '../../models/money-transfer';
import {ICbbObj} from '../../models/cbb-obj';
import {AccountService} from '../account/account.service';
import {IAccount} from '../../models/account';
import {RsaKey} from '../../shared/constant/rsa-key';
import {DataBuilderUtil} from '../../shared/util/data-builder-util';
import {AesService} from '../../service/aes.service';
import {RsaService} from '../../service/rsa.service';

@Component({
  selector: 'app-money-transfer-update',
  templateUrl: './money-transfer-update.component.html',
})
export class MoneyTransferUpdateComponent implements OnInit {
  isSaving = false;
  submitted = false;
  loading = false;

  listAccount: ICbbObj[];

  moneyTransferId;
  moneyTransfer: IMoneyTransfer;

  senderId: number;
  receiverId: number;

  editForm = this.fb.group({
    id: [],
    sender: ['', Validators.required],
    receiver: ['', Validators.required],
    amount: ['', Validators.required],
    status: ['', Validators.required],
    note: [],
  });

  constructor(protected activatedRoute: ActivatedRoute,
              private fb: FormBuilder,
              protected moneyTransferService: MoneyTransferService,
              protected accountService: AccountService,
              private aesService: AesService,
              private rsaService: RsaService,
  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      this.moneyTransferId = params.get('id');
    });


    this.accountService.getCbbData().subscribe(res => {
      if (res && res.body && res.body.data) {
        this.listAccount = res.body.data;
        this.moneyTransferService.find(this.moneyTransferId).subscribe((result) => {
          if (result && result.body && result.body.data) {
            const dataBuilder = new DataBuilderUtil(result.body.data);
            this.aesService.secretKey = this.rsaService.decrypt(dataBuilder.getAesKeyEncrypted());
            const decryptedData = this.aesService.decrypt(dataBuilder.getDataEncrypted());
            this.moneyTransfer = JSON.parse(decryptedData);
          }
          // console.log(result);
          console.log(this.moneyTransfer);
          console.log(this.listAccount);
          this.updateForm(this.moneyTransfer);

        });
      }

    });
  }


  previousState(): void {
    window.history.back();
  }

  private createFromForm(): Promise<IMoneyTransfer> {

    return new Promise<IMoneyTransfer>(resolve => {
      // const saved = new MoneyTransfer();
      this.getAccountFromId(this.senderId).then(resolve1 => {
        this.moneyTransfer.sender = resolve1;
        this.getAccountFromId(this.receiverId).then(resolve2 => {
          this.moneyTransfer.receiver = resolve2;
          this.moneyTransfer.amount = this.editForm.get(['amount'])!.value;
          this.moneyTransfer.status = this.editForm.get(['status'])!.value;
          this.moneyTransfer.note = this.editForm.get(['note'])!.value;
          resolve(this.moneyTransfer);
        });
      });
    });

  }

  getAccountFromId(id): Promise<IAccount> {
    return new Promise<IAccount>(resolve => {
      this.accountService.find(id).subscribe(res => {
        if (res && res.body && res.body.data) {
          resolve(res.body.data);
        }
      });
    });
  }

  save(): void {
    if (this.editForm.invalid) {
      return;
    }
    this.isSaving = true;

    this.createFromForm().then(resolve => {
      const dataStr = JSON.stringify(resolve);
      const keyEncrypt = this.rsaService.encrypt(this.aesService.secretKey, RsaKey.getServerPublicKey());
      console.log(RsaKey.getServerPublicKey());
      const dataEncrypt = this.aesService.encrypt(dataStr);

      const sendData = DataBuilderUtil.buildData(keyEncrypt, dataEncrypt);

      this.subscribeToSaveResponse(this.moneyTransferService.update(sendData));
    });
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMoneyTransfer>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  updateForm(moneyTransfer: IMoneyTransfer): void {
    this.editForm.patchValue({
      id: moneyTransfer.id,
      sender: moneyTransfer.sender.id,
      receiver: moneyTransfer.receiver.id,
      amount: moneyTransfer.amount,
      status: moneyTransfer.status,
      note: moneyTransfer.note,
    });
    this.senderId = moneyTransfer.sender.id;
    this.receiverId = moneyTransfer.receiver.id;
  }

  get f() {
    return this.editForm.controls;
  }
}
