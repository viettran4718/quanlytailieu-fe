import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';
import {MoneyTransferService} from './money-transfer.service';
import {IMoneyTransfer} from '../../models/money-transfer';
import {ICbbObj} from '../../models/cbb-obj';
import {AccountService} from '../account/account.service';
import {DataBuilderUtil} from '../../shared/util/data-builder-util';
import {AesService} from '../../service/aes.service';
import {RsaService} from '../../service/rsa.service';

@Component({
  selector: 'app-money-transfer-detail',
  templateUrl: './money-transfer-detail.component.html',
})
export class MoneyTransferDetailComponent implements OnInit {
  isSaving = false;
  submitted = false;
  loading = false;

  listAccount: ICbbObj[];

  moneyTransferId;
  moneyTransfer: IMoneyTransfer;

  constructor(protected activatedRoute: ActivatedRoute,
              protected moneyTransferService: MoneyTransferService,
              private aesService: AesService,
              private rsaService: RsaService,
  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      this.moneyTransferId = params.get('id');
    });

    this.moneyTransferService.find(this.moneyTransferId).subscribe((result) => {
      if (result && result.body && result.body.data) {
        const dataBuilder = new DataBuilderUtil(result.body.data);
        this.aesService.secretKey = this.rsaService.decrypt(dataBuilder.getAesKeyEncrypted());
        const decryptedData = this.aesService.decrypt(dataBuilder.getDataEncrypted());
        this.moneyTransfer = JSON.parse(decryptedData);
      }

    });
  }


  previousState(): void {
    window.history.back();
  }

}
