import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';

import {SERVER_API_URL} from '../../shared/constant/app.constants';
import {createRequestOption} from '../../shared/util/request-util';
import {IMoneyTransfer} from '../../models/money-transfer';
import {DataTransfer, IDataTransfer} from '../../models/data-transfer';

type EntityResponseType = HttpResponse<DataTransfer<string>>;

@Injectable({providedIn: 'root'})
export class MoneyTransferService {
  public resourceUrl = SERVER_API_URL + '/api/money-transfer';

  constructor(protected http: HttpClient) {
  }

  create(dataEncrypted: string): Observable<EntityResponseType> {
    return this.http.post<DataTransfer<string>>(this.resourceUrl, dataEncrypted, {observe: 'response'});
  }

  update(dataEncrypted: string): Observable<EntityResponseType> {
    return this.http.put<DataTransfer<string>>(this.resourceUrl, dataEncrypted, {observe: 'response'});
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<DataTransfer<string>>(`${this.resourceUrl}/${id}`, {observe: 'response'});
  }

  query(req?: any): Observable<EntityResponseType> {
    const options = createRequestOption(req);
    return this.http.get<DataTransfer<string>>(this.resourceUrl, {params: options, observe: 'response'});
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, {observe: 'response'});
  }
}
