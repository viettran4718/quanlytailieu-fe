import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SERVER_API_URL} from '../../shared/constant/app.constants';
import {IUser} from '../../models';

@Component({
  selector: 'app-my-info',
  templateUrl: './my-info.component.html',
  styleUrls: ['./my-info.component.css']
})
export class MyInfoComponent implements OnInit {

  user: IUser;
  roles = '';

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.getInfo().subscribe((result) => {
      this.user = result;
      for (const i in this.user.roles) {
        this.roles += this.user.roles[i]['roleName'] + ', ';
      }
      this.roles = this.roles.substr(0, this.roles.length - 2);
    });
  }

  getInfo() {
    return this.http
      .get<IUser>(SERVER_API_URL + '/api/user/myinfo');
  }

}
