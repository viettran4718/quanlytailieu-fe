import {Component, OnInit} from '@angular/core';

import {Subscription} from 'rxjs';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
// import {DocumentDeleteDialogComponent} from '../documents/document-delete-dialog.component';
import {DocumentService} from './document.service';
import {IDocument} from '../../models/document';
import {SearchWithPagination, SearchWithPaginationCls} from '../../shared/util/request-util';
import {AesService} from '../../service/aes.service';
import {RsaService} from '../../service/rsa.service';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.css']
})
export class DocumentComponent implements OnInit {
  totalRecords = 0;
  req: SearchWithPagination;
  isLoading = false;
  documents?: IDocument[];
  eventSubscriber?: Subscription;

  constructor(protected documentService: DocumentService, protected modalService: NgbModal, private aesService: AesService, private rsaService: RsaService) {
  }

  loadAll(): void {
    this.isLoading = true;
    this.documentService.query(this.req).subscribe(res => {
      const encryptedKey = res.body.otherInformation.key;
      const key = this.rsaService.decrypt(encryptedKey);
      console.log(key);
      this.aesService.secretKey = key;
      const decryptedData = this.aesService.decrypt(res.body.data);
      this.documents = JSON.parse(decryptedData) || [];
      this.totalRecords = res.body.otherInformation['X-Total-Count'];
      this.isLoading = false;
    });
  }

  ngOnInit(): void {
    this.req = new SearchWithPaginationCls();
    this.req.page = 0;
    this.req.size = 10;
    // this.req = new SearchWithPaginationCls();
    this.loadAll();
  }


  trackId(index: number, item: IDocument): number {
    return item.id;
  }

  onPaginateChange(event){
    // this.req.page = event.pageIndex;
    // this.req.size = event.pageSize;
    this.loadAll();
    // alert(JSON.stringify("Current page index: " + event.pageIndex));
  }

}
