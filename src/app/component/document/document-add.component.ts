import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {DocumentService} from './document.service';
import {SERVER_API_URL} from '../../shared/constant/app.constants';

@Component({
  selector: 'app-document-add',
  templateUrl: './document-add.component.html',
})
export class DocumentAddComponent implements OnInit {

  urlFile: string;
  multiFile: FileList;
  file: File;
  // editForm = this.fb.group({});
  editForm = this.fb.group({
    file: [],
  });

  constructor(protected activatedRoute: ActivatedRoute,
              private fb: FormBuilder,
              protected documentService: DocumentService) {
  }

  ngOnInit(): void {
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    console.log('Saved');
  }

  selectFile(event): void {
    this.multiFile = event.target.files;
  }

  upload() {
    this.file = this.multiFile.item(0);
    this.documentService.postFile(this.file).subscribe(result => {
      this.urlFile = SERVER_API_URL + result;
      console.log(this.urlFile);
    });
    console.log('Ahihi');
  }

  getFileName(urlFile: string) {
    const spl = urlFile.split('/');
    return spl[spl.length - 1];
  }

}
