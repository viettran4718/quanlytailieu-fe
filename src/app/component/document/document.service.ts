import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';

import {SERVER_API_URL} from '../../shared/constant/app.constants';
import {createRequestOption} from '../../shared/util/request-util';
import {IDocument} from '../../models/document';
import {IMyString} from '../../models/my-string';
import {DataTransfer} from '../../models/data-transfer';

type EntityResponseType = HttpResponse<IDocument>;
type EntityArrayResponseType = HttpResponse<DataTransfer<any>>;

@Injectable({providedIn: 'root'})
export class DocumentService {
  public resourceUrl = SERVER_API_URL + '/api/admin/document';
  private httpHeader: HttpHeaders;

  constructor(protected http: HttpClient) {

  }

  create(document: IDocument): Observable<EntityResponseType> {
    return this.http.post<IDocument>(this.resourceUrl, document, {observe: 'response', headers: this.httpHeader});
  }

  update(document: IDocument): Observable<EntityResponseType> {
    return this.http.put<IDocument>(this.resourceUrl, document, {observe: 'response', headers: this.httpHeader});
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IDocument>(`${this.resourceUrl}/${id}`, {observe: 'response', headers: this.httpHeader});
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<DataTransfer<any>>(this.resourceUrl, {params: options, observe: 'response', headers: this.httpHeader});
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, {observe: 'response', headers: this.httpHeader});
  }

  postFile(fileToUpload: File): Observable<any> {
    const endpoint = SERVER_API_URL + '/upload';
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload);

    // const req = new HttpRequest('POST', endpoint, formData, {
    //   reportProgress: true,
    //   responseType: 'json'
    // });
    // return this.http.request(req);

    return this.http.post(endpoint, formData, {reportProgress: true, responseType: 'text'});
  }



}
