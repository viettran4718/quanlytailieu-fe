import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';

import {SERVER_API_URL} from '../../shared/constant/app.constants';
import {createRequestOption} from '../../shared/util/request-util';
import {IAccount} from '../../models/account';
import {DataTransfer} from '../../models/data-transfer';
import {ICbbObj} from '../../models/cbb-obj';

type EntityResponseType = HttpResponse<DataTransfer<IAccount>>;
type EntityArrayResponseType = HttpResponse<DataTransfer<IAccount[]>>;

@Injectable({providedIn: 'root'})
export class AccountService {
  public resourceUrl = SERVER_API_URL + '/api/account';

  constructor(protected http: HttpClient) {

  }

  create(account: IAccount): Observable<EntityResponseType> {
    return this.http.post<DataTransfer<IAccount>>(this.resourceUrl, account, {observe: 'response'});
  }

  update(account: IAccount): Observable<EntityResponseType> {
    return this.http.put<DataTransfer<IAccount>>(this.resourceUrl, account, {observe: 'response'});
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<DataTransfer<IAccount>>(`${this.resourceUrl}/${id}`, {observe: 'response'});
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<DataTransfer<IAccount[]>>(this.resourceUrl, {params: options, observe: 'response'});
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, {observe: 'response'});
  }

  getCbbData(keySearch?: string): Observable<HttpResponse<DataTransfer<ICbbObj[]>>> {
    const options: HttpParams = new HttpParams();
    if (keySearch) {
      options.set('keySearch', keySearch);
    }
    return this.http.get<DataTransfer<ICbbObj[]>>(this.resourceUrl + '/get-cbb-data', {params: options, observe: 'response'});
  }


}
