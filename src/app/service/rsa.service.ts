import {Injectable} from '@angular/core';
import {RsaKey} from '../shared/constant/rsa-key';

declare var JSEncrypt;

@Injectable({
  providedIn: 'root'
})
export class RsaService {

  // // public jsE;
  // public publicKey;
  // public privateKey;
  // rsaKey = new RsaKey();
  constructor() {
    // this.jsE = new JSEncrypt();

  }

  public decrypt(data: string) {
    const crypt = new JSEncrypt();
    crypt.setPublicKey(RsaKey.getPublicKey());
    crypt.setPrivateKey(RsaKey.getPrivateKey());
    return crypt.decrypt(data);
  }

  public encrypt(data: string, publicKey: string) {
    const crypt = new JSEncrypt();
    crypt.setPublicKey(publicKey);
    return crypt.encrypt(data);
  }

}
