import {Injectable} from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})

export class AesService {

  public secretKey;

  constructor() {
    this.secretKey = this.genarateRandomSecret(16);
    console.log(this.secretKey);

  }

  public encrypt(plainText: string) {
    const key = CryptoJS.enc.Utf8.parse(this.secretKey);
    const inverse = CryptoJS.enc.Utf8.parse(this.secretKey);
    const encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(plainText.toString()), key,
      {
        keySize: 128,
        iv: inverse,
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7
      });
    console.log(key);
    console.log(encrypted.toString());
    return encrypted.toString();
  }

  public decrypt(encryptedText: string) {
    // console.log(this.getSignatureFromToken());
    const key = CryptoJS.enc.Utf8.parse(this.secretKey);
    const inverse = CryptoJS.enc.Utf8.parse(this.secretKey);
    const decrypted = CryptoJS.AES.decrypt(encryptedText, key, {
      keySize: 128,
      iv: inverse,
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    });
    // console.log(decrypted.toString(CryptoJS.enc.Utf8));
    return decrypted.toString(CryptoJS.enc.Utf8);
  }

  public genarateRandomSecret(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
}
