import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
// import {GlobalVariable} from '../global';
import { SERVER_API_URL } from '../shared/constant/app.constants';

import {User} from '../models';
import {Login} from '../component/login/login.model';

type JwtToken = {
  token: string;
  publicKey: string;
};

@Injectable({providedIn: 'root'})
export class AuthenticationService {
  public credential: Login;
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(sessionStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
    this.credential = new Login();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  // login(credentials: Login): Observable<void> {
  //   return this.http
  //     .post<JwtToken>(SERVER_API_URL + 'api/authenticate', credentials)
  //     .pipe(map(response => this.authenticateSuccess(response, credentials.rememberMe)));
  // }

  login(username, password) {
    this.credential.password = password;
    this.credential.username = username;
    return this.http
      .post<JwtToken>(SERVER_API_URL + '/api/user/login', this.credential)
      .pipe(map(response => this.authenticateSuccess(response)));
    // this.http
    //   .post<JwtToken>(SERVER_API_URL + '/api/user/login', this.credential)
    //   .subscribe(data => {
    //     console.log(data);
    //   });
    // return null;
  }

  logout(): Observable<void> {
    return new Observable(observer => {
      sessionStorage.removeItem('authenticationToken');
      observer.complete();
    });
  }

  private authenticateSuccess(response: JwtToken): void {
    sessionStorage.setItem('authenticationToken', response.token);
    sessionStorage.setItem('publicKeyServer', response.publicKey);
  }
}
